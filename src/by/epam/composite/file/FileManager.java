package by.epam.composite.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class FileManager {

	private final static Logger LOG = Logger.getLogger(FileManager.class);

	private File newText = new File("text\\new_text.txt");
	private File propertiesFile = new File("src\\resources\\regex.properties");

	public Properties getRegex() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(propertiesFile));
		} catch (FileNotFoundException e) {
			LOG.warn("Unable to find properties file");
		} catch (IOException e) {
			LOG.warn("Unable to manipulate properties file");
		}
		return properties;
	}

	public FileWriter createFileWriter() {
		FileWriter fw = null;
		try {
			fw = new FileWriter(newText);
			fw.write("New Text File:");
			fw.write(System.lineSeparator());
			fw.write(System.lineSeparator());
		} catch (IOException e) {
			LOG.warn("Unable to write text");
		}
		return fw;
	}

	public String initialization(String path) {

		String text = null;

		try {
			@SuppressWarnings("resource")
			FileInputStream inFile = new FileInputStream(path);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			text = new String(str);
		} catch (FileNotFoundException e) {
			LOG.warn("Unable to find the text file");
		} catch (IOException e) {
			LOG.warn("Unable to manipulate the text file");
		}
		return text;
	}

	public void writeText(FileWriter fw, StringBuilder sb) {
		try {
			fw.write("Assembled Text:");
			fw.write(System.lineSeparator());
			fw.write(System.lineSeparator());
			fw.write(sb.toString());
		} catch (IOException e) {
			LOG.warn("Unable to write text");
		}
	}

	public void writeSortedWords(FileWriter fw, StringBuilder sb) {
		try {
			fw.write(System.lineSeparator());
			fw.write(System.lineSeparator());
			fw.write("Sorted Words:");
			fw.write(System.lineSeparator());
			fw.write(System.lineSeparator());
			fw.write(sb.toString());
		} catch (IOException e) {
			LOG.warn("Unable to write text");
		}
	}

	public void closeWriter(FileWriter fw) {
		if (fw != null) {
			try {
				fw.close();
			} catch (IOException e) {
				LOG.warn("Closing file exception");
			}
		}
	}

}
