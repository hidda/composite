package by.epam.composite.parser;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.composite.entity.ComponentType;
import by.epam.composite.entity.LeafComponent;
import by.epam.composite.entity.PunctComponent;
import by.epam.composite.entity.TextComponent;
import by.epam.composite.entity.WordComponent;
import by.epam.composite.file.FileManager;

public class TextParser {

	public TextParser() {
	}

	public TextComponent parse(String path, FileManager fm,
			Properties regexProperties) {
		String textFile = fm.initialization(path);
		TextComponent text = new TextComponent();
		text.setType(ComponentType.TEXT);
		parseToParagraph(text, textFile, regexProperties);
		return text;
	}

	private TextComponent parseToParagraph(TextComponent text, String textFile,
			Properties regexProperties) {
		TextComponent paragraphList = new TextComponent();
		paragraphList.setType(ComponentType.PARAGRAPH);
		Pattern paragraphPattern = Pattern.compile(regexProperties.getProperty("paragraph"));
		String paragraph;
		Matcher matcher = paragraphPattern.matcher(textFile);
		while (matcher.find()) {
			paragraph = matcher.group();
			if (Pattern.matches(regexProperties.getProperty("code"), paragraph)) {
				LeafComponent leafComponent = new LeafComponent(paragraph);
				paragraphList.add(leafComponent);
			} else {
				parseToSentense(paragraphList, paragraph, regexProperties);
			}
		}
		text.add(paragraphList);
		return text;
	}

	private TextComponent parseToSentense(TextComponent paragraphList,
			String paragraph, Properties regexProperties) {
		String sentence;
		Pattern sentencePattern = Pattern.compile(regexProperties.getProperty("sentence"));
		Matcher matcher = sentencePattern.matcher(paragraph);
		TextComponent sentenceList = new TextComponent();
		sentenceList.setType(ComponentType.SENTENCE);
		while (matcher.find()) {
			sentence = matcher.group();
			parseToLexemes(sentenceList, sentence, regexProperties);
		}
		paragraphList.add(sentenceList);
		return sentenceList;
	}

	private TextComponent parseToLexemes(TextComponent sentenceList,
			String sentence, Properties regexProperties) {
		Pattern lexemePattern = Pattern.compile(regexProperties.getProperty("lexeme"));
		Matcher matcher = lexemePattern.matcher(sentence);
		TextComponent lexemesList = new TextComponent();
		lexemesList.setType(ComponentType.LEXEME);
		String lexeme;
		while (matcher.find()) {
			lexeme = matcher.group();
			parseToWords(lexemesList, lexeme, regexProperties);
		}
		sentenceList.add(lexemesList);
		return lexemesList;
	}

	private TextComponent parseToWords(TextComponent lexemesList, String lexeme, Properties regexProperties) {
		Pattern punctPattern = Pattern.compile(regexProperties.getProperty("punct"));
		Pattern wordPattern = Pattern.compile(regexProperties.getProperty("word"));
		Matcher matcher = wordPattern.matcher(lexeme);
		Matcher punctMatcher = punctPattern.matcher(lexeme);
		String word;
		String punct;
		if (matcher.find()) {
			word = matcher.group();
			WordComponent wordLeaf = new WordComponent(word);
			lexemesList.add(wordLeaf);
		}
		if (punctMatcher.find()) {
			punct = punctMatcher.group();
			PunctComponent punctLeaf = new PunctComponent(punct);
			lexemesList.add(punctLeaf);
		}
		return lexemesList;
	}

}