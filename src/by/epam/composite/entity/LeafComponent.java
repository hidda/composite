package by.epam.composite.entity;

import java.util.List;

public class LeafComponent implements IComponent {

	private String str;
	private static ComponentType type = ComponentType.LISTING;

	public static ComponentType getType() {
		return type;
	}

	public LeafComponent(String str) {
		this.str = str;
	}

	@Override
	public void add(IComponent component) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void remove(IComponent component) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IComponent takeElement(int index) {
		return this;
	}

	@Override
	public String toString() {
		return str;
	}

	@Override
	public StringBuilder extractText(StringBuilder sb) {
		sb.deleteCharAt(sb.length()-1);
		sb.append(this.str);
		return sb;
	}

	@Override
	public List<IComponent> extractWords(List<IComponent> list) {
		return list;
	}

}
