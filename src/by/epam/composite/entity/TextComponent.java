package by.epam.composite.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TextComponent implements IComponent {

	private List<IComponent> componentList = new ArrayList<IComponent>();
	private ComponentType type;

	public ComponentType getType() {
		return type;
	}

	public void setType(ComponentType type) {
		this.type = type;
	}

	@Override
	public IComponent takeElement(int index) {
		return componentList.get(index);
	}

	@Override
	public void add(IComponent component) {
		componentList.add(component);
	}

	@Override
	public void remove(IComponent component) {
		componentList.remove(component);
	}

	public List<IComponent> getComponentList() {
		return Collections.unmodifiableList(componentList);
	}

	@Override
	public StringBuilder extractText(StringBuilder sb) {
		ComponentType type = this.getType();
		int size = componentList.size();
		for (int i = 0; i < size; i++) {
			switch (type) {
			case PARAGRAPH:
				if (i != 0) {
					sb.append(System.lineSeparator());
					sb.append(System.lineSeparator());
				}
				break;

			default:
				break;
			}
			componentList.get(i).extractText(sb);
		}
		return sb;
	}

	@Override
	public List<IComponent> extractWords(List<IComponent> list) {
		int size = componentList.size();
		for (int i = 0; i < size; i++) {
			componentList.get(i).extractWords(list);
		}
		return list;
	}

}
