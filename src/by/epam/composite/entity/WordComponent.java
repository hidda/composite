package by.epam.composite.entity;

import java.util.List;

public class WordComponent implements IComponent {

	private String word;
	private static ComponentType type = ComponentType.WORD;

	public ComponentType getType() {
		return type;
	}

	public WordComponent(String word) {
		this.word = word;
	}

	@Override
	public String toString() {
		return word;
	}

	@Override
	public void add(IComponent component) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void remove(IComponent component) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IComponent takeElement(int index) {
		return this;
	}

	@Override
	public StringBuilder extractText(StringBuilder sb) {
		sb.append(this.word);
		sb.append(" ");
		return sb;
	}

	@Override
	public List<IComponent> extractWords(List<IComponent> list) {
		list.add(this);
		return list;
	}
}
