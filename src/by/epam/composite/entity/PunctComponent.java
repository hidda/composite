package by.epam.composite.entity;

import java.util.List;

public class PunctComponent implements IComponent {

	private String punct;
	private static ComponentType type = ComponentType.PUNCT;

	public static ComponentType getType() {
		return type;
	}

	public PunctComponent(String punct) {
		this.punct = punct;
	}

	@Override
	public void add(IComponent component) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void remove(IComponent component) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IComponent takeElement(int index) {
		return this;
	}

	@Override
	public String toString() {
		return punct;
	}

	@Override
	public StringBuilder extractText(StringBuilder sb) {

		switch (this.punct) {
		case "-":
			sb.deleteCharAt(sb.length() - 1);
			sb.append(this.punct);
			break;

		case "(":
			sb.append(this.punct);
			break;

		default:
			sb.deleteCharAt(sb.length() - 1);
			sb.append(this.punct);
			sb.append(" ");
		}
		return sb;
	}

	@Override
	public List<IComponent> extractWords(List<IComponent> list) {
		return list;
	}
}
