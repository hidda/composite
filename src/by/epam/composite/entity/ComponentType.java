package by.epam.composite.entity;

public enum ComponentType {
	
	TEXT,
	PARAGRAPH, 
	LISTING,
	SENTENCE,
	LEXEME,
	WORD,
	PUNCT;

}
