package by.epam.composite.entity;

import java.util.List;

public interface IComponent {

	void add(IComponent component);

	void remove(IComponent component);

	IComponent takeElement(int index);

	StringBuilder extractText(StringBuilder sb);

	List<IComponent> extractWords(List<IComponent> list);
}
