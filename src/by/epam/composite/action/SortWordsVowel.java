package by.epam.composite.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.composite.entity.IComponent;

public class SortWordsVowel {

	// Sorts words in the text by the proportion of vowel letters in them
	public static StringBuilder sort(IComponent text, final Properties properties) {
		List<IComponent> wordList = new ArrayList<IComponent>();
		wordList = text.extractWords(wordList);

		Comparator<IComponent> comparator = new Comparator<IComponent>() {
			@Override
			public int compare(IComponent wordOne, IComponent wordTwo) {
				return Double.compare(vowelCount(wordOne, properties), vowelCount(wordTwo, properties));
			}
		};

		Collections.sort(wordList, comparator);
		StringBuilder sb = new StringBuilder();
		sb = keepSortedWords(wordList, sb, properties);
		return sb;
	}

	// Calculates the proportion of vowel letters
	private static double vowelCount(IComponent word, Properties properties) {
		Pattern vowelPattern = Pattern.compile(properties.getProperty("vowel"));
		Matcher matcher = vowelPattern.matcher(word.toString());
		int count = 0;
		while (matcher.find()) {
			count++;
		}
		double vowelProportion = (double) count / word.toString().length();
		return vowelProportion;
	}

	private static StringBuilder keepSortedWords(List<IComponent> wordList,
			StringBuilder sb, Properties properties) {
		for (IComponent word : wordList) {
			sb.append(word.toString());
			sb.append(" ");
			sb.append("Vowel Proportion: " + vowelCount(word, properties));
			sb.append(System.lineSeparator());
		}
		return sb;
	}

}
