package by.epam.composite.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import by.epam.composite.entity.IComponent;

public class SortWordsAlphabet {

	// Sorts words in the text by the first letter
	public static StringBuilder sort(IComponent text) {
		List<IComponent> wordList = new ArrayList<IComponent>();
		wordList = text.extractWords(wordList);

		Comparator<IComponent> comparator = new Comparator<IComponent>() {
			@Override
			public int compare(IComponent wordOne, IComponent wordTwo) {
				return wordOne.toString().compareToIgnoreCase(
						wordTwo.toString());
			}
		};

		Collections.sort(wordList, comparator);
		StringBuilder sb = new StringBuilder();
		sb = keepSortedWords(wordList, sb);
		return sb;
	}

	// Words, which start from the same letter, are kept in the same line. If
	// the first letter is different, sb keeps them in the new line.
	private static StringBuilder keepSortedWords(List<IComponent> wordList,
			StringBuilder sb) {
		for (int i = 1; i < wordList.size(); i++) {
			String wordOne = wordList.get(i - 1).toString().toLowerCase();
			String wordTwo = wordList.get(i).toString().toLowerCase();
			if (wordOne.charAt(0) == wordTwo.charAt(0)) {
				sb.append(wordOne);
				sb.append(" ");
			} else {
				sb.append(wordOne);
				sb.append(System.lineSeparator());
			}
			if (i == wordList.size() - 1) {
				sb.append(wordTwo);
			}
		}
		return sb;
	}

}