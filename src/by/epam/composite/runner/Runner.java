package by.epam.composite.runner;

import java.io.FileWriter;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import by.epam.composite.action.SortWordsVowel;
import by.epam.composite.action.SortWordsAlphabet;
import by.epam.composite.entity.IComponent;
import by.epam.composite.file.FileManager;
import by.epam.composite.parser.TextParser;

public class Runner {

	private final static String CONFIG_NAME = "config/log4j.xml";
	private final static String FILE_PATH = "text//test_text.txt";

	static {
		new DOMConfigurator().doConfigure(CONFIG_NAME,
				LogManager.getLoggerRepository());
	}

	public static void main(String[] args) {

		FileManager fm = new FileManager();
		FileWriter fw = fm.createFileWriter();
		TextParser parser = new TextParser();
		Properties properties = fm.getRegex();
		
		IComponent text = parser.parse(FILE_PATH, fm, properties);
		StringBuilder sb = new StringBuilder();
		sb = text.extractText(sb);
		fm.writeText(fw, sb);
		
		StringBuilder sb2 = SortWordsAlphabet.sort(text);
		fm.writeSortedWords(fw, sb2);
		
		StringBuilder sb3 = SortWordsVowel.sort(text, properties);
		fm.writeSortedWords(fw, sb3);

		fm.closeWriter(fw);
	}
}
